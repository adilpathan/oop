package oop;

public class Bike extends Vehicle {
	
	private boolean fourStroke = true;
	
	public Bike(String name,String color,String model,String company,String engine,boolean fourStroke) 
	{
		super(name,color,model,company,engine);
		this.fourStroke = fourStroke;
		
	}
	
	public void setFourStroke(boolean fourStroke) {
		this.fourStroke = fourStroke;
	}
	
	public String getFourStroke() {
		if(this.fourStroke==true) return "The vehicle is 4-stroke";
		else return "The vehicle is not 4-stroke";
	}
	
	public String getInfo() {
		return "This is a bike";
	}

}
